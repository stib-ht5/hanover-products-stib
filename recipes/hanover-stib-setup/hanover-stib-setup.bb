DESCRIPTION = "First boot setup and extra hackery"
LICENSE = "MIT"
PN = "hanover-${PRODUCT}-setup"
PV = "5.1"

SRC_URI =  " \
	file://setup.sh \
"

RDEPENDS = "hanover-configs"
PACKAGES = "${PN}"

COMPATIBLE_MACHINE = "(dm365-htc|dm814x-ht5)"
PACKAGE_ARCH = "${MACHINE_ARCH}"

do_configure() {
	:
}

do_compile() {
	:
}

do_install () {
	mkdir -p ${D}${sysconfdir}/setup.d
	install -m 0755 ${WORKDIR}/setup.sh ${D}${sysconfdir}/setup.d/prepare-${PRODUCT}
}

FILES_${PN} = "${sysconfdir}"
