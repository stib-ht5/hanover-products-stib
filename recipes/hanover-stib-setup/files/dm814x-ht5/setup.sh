#!/bin/sh

# http://swdev/issues/1761
ln -snf /sys/bus/platform/devices/hdl-ht5-ec-keypad/ignition /var/run/ignition

echo 0 > /var/volatile/tmp/timeok

# setconfig will create /etc/patches
# as a symlink to configs/*/patches
# to indicate it has been configured.
if [ ! -e /etc/patches/ ]; then
	setconfig stib.default.static.ip.2
fi
