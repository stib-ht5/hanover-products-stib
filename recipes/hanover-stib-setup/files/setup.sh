#!/bin/sh

# setconfig will create /etc/patches
# as a symlink to configs/*/patches
# to indicate it has been configured.
if [ ! -e /etc/patches/ ]; then
	setconfig stib.default.static.ip.2
fi
