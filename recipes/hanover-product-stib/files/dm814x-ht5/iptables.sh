#!/bin/sh

set -eu

case "$IFACE" in
eth1)
	case "$MODE" in
	start)
		sysctl -n -w net.ipv4.ip_forward=1

		# SNMP Port Forwarding
		for port in 161 162 1993; do
			for proto in tcp udp; do
				iptables -t nat -A PREROUTING -i $IFACE -p $proto --dport $(($port + 8000)) \
					-j DNAT --to-destination 192.168.9.200:$port
			done
		done
		;;
	stop)
		iptables -t nat -F PREROUTING
		;;
	esac
	;;
esac
