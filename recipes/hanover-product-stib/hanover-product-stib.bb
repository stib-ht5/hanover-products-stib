#Hanover configuration files package
# vim: set ft=sh noet ts=8 sw=8:

LICENSE = "MIT"

inherit srctree gitver

PACKAGES = "${PN}"
PACKAGE_ARCH = "${MACHINE_ARCH}"

PV = ${GITVER}
S  = ${FILE_DIRNAME}/../..
PR = "r3"

RDEPENDS = " \
	hanover-${PRODUCT}-setup \
	hanover-${PRODUCT}-version \
	"

RRECOMMENDS_${PN} += "\
	task-hanover-${PRODUCT}	\
	${PN}-configs		\
	"

do_configure () {
  :
}
 
do_compile () {
  :
}

do_install[dirs] = "${FILE_DIRNAME}"

do_install () {
	local c= d= f=

	# ${PN}
	#
	# build environment variables
	install -d ${D}${sysconfdir}
	cat <<-EOT > ${D}${sysconfdir}/hanover.conf
	export RELDIR=${RELDIR}
	export BUILD_PURPOSE=${BUILD_PURPOSE}
	export PRODUCT=${PRODUCT}
	export PRODUCT_RELEASE=${PRODUCT_RELEASE}
	export MACHINE=${MACHINE}
	export FEED_ARCH=${FEED_ARCH}
	EOT
   
	# ${PN}-configs
	#
	for c in configs/*; do
		if [ -f "$c/description.txt" ]; then
			d=${D}${sysconfdir}/$c
			mkdir -p "$d"
			install -m 0644 $c/description.txt $d/

			mkdir "$d/patches"
			for f in $c/patches/*.diff \
				 $c/patches/machine/${MACHINE}/*.diff \
				 ; do
				if [ -s "$f" ]; then
					install -m 0644 "$f" "$d/patches/"
				fi
			done
			ls -1 "$d/patches"/*.diff | sed -e 's|.*/||' > "$d/patches/series"

			mkdir "$d/scripts"
			for f in $c/scripts/* \
				 $c/scripts/machine/${MACHINE}/* \
				 ; do
				if [ ! -d "$f" -a -s "$f" ]; then
					install -m 0755 "$f" "$d/scripts/"
				fi
			done
		fi
	done
}

do_install_append_dm814x-ht5() {
	d="${D}${sysconfdir}/network"
	mkdir -p "$d/if-up.d" "$d/if-down.d"

	install -m 0755 "files/${MACHINE}/iptables.sh" "$d/if-up.d/iptables"
	ln -s ../if-up.d/iptables "$d/if-down.d/iptables"
}

FILES_${PN} =  "${sysconfdir}/hanover.conf"
FILES_${PN}_append_dm814x-ht5 += "${sysconfdir}/network"

CONFFILES_${PN} = " \
	${sysconfdir}/hanover.conf \
"

PACKAGES += "${PN}-configs"
FILES_${PN}-configs = "${sysconfdir}/configs/*"

RDEPENDS_${PN}-configs = "quilt"

do_deploy () {
	local d= bn= zip_args= suffix=
	local has_variants=false
	install -d ${DEPLOY_DIR_IMAGE}

	suffix=".${PRODUCT}.${PRODUCT_RELEASE}"
	rm -f "${DEPLOY_DIR_IMAGE}"/data*"$suffix.zip"
	for d in "${S}"/data "${S}"/data-*; do
		[ -d "$d/" ] || continue
		bn=$(basename "$d")
		zip_args="-r -X"

		if [ -s "${DEPLOY_DIR_IMAGE}/data$suffix.zip" ]; then
			cp "${DEPLOY_DIR_IMAGE}/data$suffix.zip" "${DEPLOY_DIR_IMAGE}/$bn$suffix.zip"
			zip_args="$zip_args -g"
			has_variants=true
		fi

		cd "$d"
		zip $zip_args "${DEPLOY_DIR_IMAGE}/$bn$suffix.zip" *
		ln -sf "$bn$suffix.zip" "${DEPLOY_DIR_IMAGE}/$bn.zip"
		cd - > /dev/null
	done

	# destroy "common" if we got data-* variants
	if $has_variants; then
		rm -f "${DEPLOY_DIR_IMAGE}/data.zip" "${DEPLOY_DIR_IMAGE}/data$suffix.zip"
	fi
}

do_deploy[dirs] = "${S}"
addtask deploy before do_package_stage after do_install
