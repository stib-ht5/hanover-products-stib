# vim: set ft=sh ts=8 sw=8 noet:

PE = "1"
PV = "1.2.1"
PR = "r1"
S  = ${FILE_DIRNAME}

RDEPENDS_${PN}_append = "\
	tzdata \
"

PACKAGE_ARCH = "${MACHINE_ARCH}"

REGION_LOCALE="Europe/Paris"

do_configure() {
	:
}

do_compile() {
	:
}

UPDATE=update
MONITORS="ignition_control led_control"
APPS="STIBApp templateFiller serve_templates"

do_install() {
	local item=

	# what for? -amery
	install -d "${D}${sysconfdir}/init.d"
	install -m 0755 ${S}/files/iptables_init.sh "${D}${sysconfdir}/init.d/"
	#ln -sf ../init.d/iptables_init ${D}${sysconfdir}/rc5.d/S95iptables_init

	mkdir -p "${D}/usr/local/config-data"
	install -m 0644 "${S}"/files/${MACHINE}/config-data/* "${D}/usr/local/config-data/"

	# enabled updated service
	install -d "${D}/var/service"
	for item in ${UPDATE} ${MONITORS} ${APPS}; do
		ln -sf ../run/service/$item "${D}/var/service/$item"
	done

	touch "${D}${sysconfdir}/timezone"
	chmod 0666 "${D}${sysconfdir}/timezone"

	# override current settings with desired timezone
	echo ${REGION_LOCALE} > "${D}${sysconfdir}/timezone"
	ln -sf /usr/share/zoneinfo/${REGION_LOCALE} ${D}${sysconfdir}/localtime
}

do_install_append_dm365-htc() {
	install -d "${D}${sysconfdir}/init.d"
	install -d "${D}${sysconfdir}/rc5.d"
	install -m 0755 "${S}/files/${MACHINE}/loadmodules.sh" "${D}${sysconfdir}/init.d"
	ln -sf ../init.d/loadmodules.sh "${D}${sysconfdir}/rc5.d/S99loadmodules"

	touch "${D}/safemode"
	chmod 0755 "${D}/safemode"
}

FILES_${PN} += "/var/service"
FILES_${PN} += "/usr/local/config-data"
FILES_${PN}_append_dm365-htc += "/safemode"
