#=================================================================================
#
#                              SNMP Trap Definitions. 
#
#=================================================================================
#
# This file defines all required SNMP trap definitions.
#
# A trap is potentially triggered in response to some incoming message.
#
# The receipt of an appropriate message is potentially only the first step to
# determining if the associated trap should be set. 
#
# The SNMP collective state is defined to be the values of all SNMP items
# (as defined in snm-scalars.cfg and snmp-tables.cfg). When an incoming message
# is received, this may result in a change in this state (i.e. via SNMP bindings).
#
# An optional state change condition may be defined. Post-receipt of a
# relevant trigger message, only if this state change condition is met
# will the SNMP trap be set.
#
# The state change condition is defined by an expression (which is evaluated
# w.r.t the SNMP collective state) and a trigger condition. The expression
# is actually evaluated *twice* - once before any update to the collective 
# SNMP state, and once afterwards. The before-and-after expression results
# plus the trigger type determine whether the trap is set. The trigger type
# can be any of: "always", "high", "low", "leading.edge", "trailing.edge", 
# or "state.change".
#
#  +-------------------+----------------------------------+
#  |   trigger-type    |   condition for trap to be set   |
#  |                   +- - - - - - - - +- - - - - - - - -+
#  |                   |    pre-eval    |   post-eval     |
#  +-------------------+----------------+-----------------+
#  |   always          |     n/a        |    n/a          |
#  |   high            |     n/a        |    true         |
#  |   low             |     n/a        |    false        |
#  |   leading.edge    |     false      |    true         |
#  |   trailing.edge   |     true       |    false        |
#  |   state.change    |     false      |    true         |
#  |   state.change    |     true       |    false        |
#  +-------------------+----------------------------------+
#
# Trap definitions take on the following form:
#
#     trap <trap name>
#        trap.oid <trap oid>
#        message.selector <msg class [where <expr>]>
#        trigger.condition <expr>
#        trigger.type [always | high | leading.edge | trailing.edge | state.change]
#        associated.object <bound alias>
#        [associated.object <bound alias>...]    
#     end
#
# Refer to the extensive SNMP configuration documentation for more details of the
# above and comprehensive instructions on how to edit this file.
#
# SJM, 2014-10-22
#
#=================================================================================


#=================================================================================
#
#                              EQUIPMENT FAILURE
#
#=================================================================================
#
#                   Trap: OBC process failed at some point.
#
# --------------------------------------------------------------------------------

trap dead.obc.trap

    # OID is for obcHt5Alert
    trap.oid .1.3.6.1.4.1.42536.4.2.0.1
    
    message.selector hanover.htc.device.state where (device.type = "obc")
    
    # Condition: needSwap
    trigger.condition obc.table[1, 23]
    trigger.type leading.edge
    
    # Associated: obcHtcNeedSwap
    associated.object obc.table[1, 23]
    
    # Associated: obcHtcErrorText
    associated.object obc.table[1, 24]
    
    # Associated: obcHtcVehicleID
    associated.object obc.table[1, 25]
    
    # Associated: obcHtcTimeStamp
    associated.object obc.table[1, 26]

end

# --------------------------------------------------------------------------------
#
#                           Trap: Front LED failed.
#
# --------------------------------------------------------------------------------

trap dead.led.front.trap

    # OID is for signLedAlert
    trap.oid .1.3.6.1.4.1.42536.4.3.0.1
    
    message.selector hanover.htc.led.status where (sign.name = "front.sign")
    
    # Condition: !signLedFunctioning || !signLedCommunicating
    trigger.condition !sign.table[1,10] || !sign.table[1,11] 
    trigger.type leading.edge
    
    # Associated: signLedSerialNumber
    associated.object sign.table[1,2]
    
    # Associated: signLedFaultMessages
    associated.object sign.table[1,12]
    
    # Associated: obcHtcVehicleID
    associated.object obc.table[1, 25]
    
    # Associated: obcHtcTimeStamp
    associated.object obc.table[1, 26]
    
end

# --------------------------------------------------------------------------------
#
#                           Trap: Side LED failed.
#
# --------------------------------------------------------------------------------

trap dead.led.side.trap

    # OID is for signLedAlert
    trap.oid .1.3.6.1.4.1.42536.4.3.0.1
    
    message.selector hanover.htc.led.status where (sign.name = "side.sign")

    # Condition: !signLedFunctioning || !signLedCommunicating
    trigger.condition !sign.table[2,10] || !sign.table[2,11] 
    trigger.type leading.edge
    
    # Associated: signLedSerialNumber
    associated.object sign.table[2,2]
    
    # Associated: signLedFaultMessages
    associated.object sign.table[2,12]
    
    # Associated: obcHtcVehicleID
    associated.object obc.table[1, 25]
    
    # Associated: obcHtcTimeStamp
    associated.object obc.table[1, 26]

 end

# --------------------------------------------------------------------------------
#
#                           Trap: Rear LED failed.
#
# --------------------------------------------------------------------------------

trap dead.led.rear.trap

    # OID is for signLedAlert
    trap.oid .1.3.6.1.4.1.42536.4.3.0.1
    
    message.selector hanover.htc.led.status where (sign.name = "rear.sign")

    # Condition: !signLedFunctioning || !signLedCommunicating
    trigger.condition !sign.table[3,10] || !sign.table[3,11] 
    trigger.type leading.edge
    
    # Associated: signLedSerialNumber
    associated.object sign.table[3,2]
    
    # Associated: signLedFaultMessages
    associated.object sign.table[3,12]
    
    # Associated: obcHtcVehicleID
    associated.object obc.table[1, 25]
    
    # Associated: obcHtcTimeStamp
    associated.object obc.table[1, 26]

end

# --------------------------------------------------------------------------------
#
#                             Trap: Single TFT failed.
#
# --------------------------------------------------------------------------------

trap dead.tft.single.trap

    # OID is for tftIpAlert
    trap.oid .1.3.6.1.4.1.42536.4.4.0.1
    
    message.selector hanover.htc.tft.status
    
    # Condition: tftIpNeedSwap || (tftIpHealthCode < 0)
    trigger.condition (tft.table[1, 11] = true) || (tft.table[1, 12] < 0)
    trigger.type leading.edge

    # Associated: tftIpSerialNumber
    associated.object tft.table[1, 2]
    
    # Associated: tftIpFaultMessages
    associated.object tft.table[1, 13]
    
    # Associated: obcHtcVehicleID
    associated.object obc.table[1, 25]
    
    # Associated: obcHtcTimeStamp
    associated.object obc.table[1, 26]
    
end

# --------------------------------------------------------------------------------
#
#                             Trap: Double TFT failed.
#
# --------------------------------------------------------------------------------

trap dead.tft.double.trap

    # OID is for tftIpAlert
    trap.oid .1.3.6.1.4.1.42536.4.4.0.1
    
    message.selector hanover.htc.tft.status

    # Condition: tftIpNeedSwap || (tftIpHealthCode < 0)
    trigger.condition (tft.table[2, 11] = true) || (tft.table[2, 12] < 0)
    trigger.type leading.edge
    
    # Associated: tftIpSerialNumber
    associated.object tft.table[2, 2]
    
    # Associated: tftIpFaultMessages
    associated.object tft.table[2, 13]
    
    # Associated: obcHtcVehicleID
    associated.object obc.table[1, 25]
    
    # Associated: obcHtcTimeStamp
    associated.object obc.table[1, 26]
        
end

#=================================================================================
#
#                                 UPDATE FAILURE
#
#=================================================================================
#
#                             Trap: OBC update failed.
#
# --------------------------------------------------------------------------------

trap obc.update.failed.trap

    # OID is for hanUpdAlert. For updates per se, we have just one alert.
    trap.oid .1.3.6.1.4.1.42536.4.1.0.1
    
    message.selector hanover.htc.device.update.status where (device.type = "obc")
    
    # Condition: !hanUpdSuccess
    trigger.condition !update.table[1, 6]
    trigger.type leading.edge

    # Associated: hanUpdWhen
    associated.object update.table[1, 5]

    # Associated: hanUpdDeviceType
    associated.object update.table[1, 2]

    # Associated: ht5SerialNumber (alt. could use hanUpdSerialNumber (update.table[1, 3]).
    associated.object obc.table[1, 2]

    # Associated: hanUpdUpdateType
    associated.object update.table[1, 4]

    # Associated: obcHtcVehicleID
    associated.object obc.table[1, 25]

end

# --------------------------------------------------------------------------------
#
#                        Trap: LED front sign update failed.
#
# --------------------------------------------------------------------------------

trap front.led.update.failed.trap

    # OID is for hanUpdAlert. For updates per se, we have just one alert.
    trap.oid .1.3.6.1.4.1.42536.4.1.0.1

    message.selector hanover.htc.device.update.status where (device.type = "led.sign") && (device.name = "front.sign")
    
    # Condition: !hanUpdSuccess
    trigger.condition !update.table[2, 6]
    trigger.type leading.edge

    # Associated: hanUpdWhen
    associated.object update.table[2, 5]

    # Associated: hanUpdDeviceType
    associated.object update.table[2, 2]

    # Associated: ht5SerialNumber (alt. could use hanUpdSerialNumber (update.table[2, 3]).
    associated.object sign.table[1, 2]

    # Associated: hanUpdUpdateType
    associated.object update.table[2, 4]

    # Associated: obcHtcVehicleID
    associated.object obc.table[1, 25]

end

# --------------------------------------------------------------------------------
#
#                        Trap: LED side sign update failed.
#
# --------------------------------------------------------------------------------

trap side.led.update.failed.trap

    # OID is for hanUpdAlert. For updates per se, we have just one alert.
    trap.oid .1.3.6.1.4.1.42536.4.1.0.1

    message.selector hanover.htc.device.update.status where (device.type = "led.sign") && (device.name = "side.sign")
    
    # Condition: !hanUpdSuccess
    trigger.condition !update.table[3, 6]
    trigger.type leading.edge

    # Associated: hanUpdWhen
    associated.object update.table[3, 5]

    # Associated: hanUpdDeviceType
    associated.object update.table[3, 2]

    # Associated: ht5SerialNumber (alt. could use hanUpdSerialNumber (update.table[3, 3]).
    associated.object sign.table[2, 2]

    # Associated: hanUpdUpdateType
    associated.object update.table[3, 4]

    # Associated: obcHtcVehicleID
    associated.object obc.table[1, 25]

end

# --------------------------------------------------------------------------------
#
#                        Trap: LED rear sign update failed.
#
# --------------------------------------------------------------------------------

trap rear.led.update.failed.trap

    # OID is for hanUpdAlert. For updates per se, we have just one alert.
    trap.oid .1.3.6.1.4.1.42536.4.1.0.1

    message.selector hanover.htc.device.update.status where (device.type = "led.sign") && (device.name = "rear.sign")
    
    # Condition: !hanUpdSuccess
    trigger.condition !update.table[4, 6]
    trigger.type leading.edge

    # Associated: hanUpdWhen
    associated.object update.table[4, 5]

    # Associated: hanUpdDeviceType
    associated.object update.table[4, 2]

    # Associated: ht5SerialNumber (alt. could use hanUpdSerialNumber (update.table[4, 3]).
    associated.object sign.table[3, 2]

    # Associated: hanUpdUpdateType
    associated.object update.table[4, 4]

    # Associated: obcHtcVehicleID
    associated.object obc.table[1, 25]

end

# --------------------------------------------------------------------------------
#
#                         Trap: Single TFT update failed.
#
# --------------------------------------------------------------------------------

trap tft.single.update.failed.trap

    # OID is for hanUpdAlert. For updates per se, we have just one alert.
    trap.oid .1.3.6.1.4.1.42536.4.1.0.1
    
    message.selector hanover.htc.device.update.status where (device.type = "tft") && (device.name = "tft.single")

    # Condition: !hanUpdSuccess
    trigger.condition !update.table[5, 6]
    trigger.type leading.edge
    
    # Associated: hanUpdWhen
    associated.object update.table[5, 5]

    # Associated: hanUpdDeviceType
    associated.object update.table[5, 2]

    # Associated: ht5SerialNumber (alt. could use hanUpdSerialNumber (update.table[5, 3]).
    associated.object tft.table[1, 2]

    # Associated: hanUpdUpdateType
    associated.object update.table[5, 4]

    # Associated: obcHtcVehicleID
    associated.object obc.table[1, 25]

end

# --------------------------------------------------------------------------------
#
#                         Trap: Double TFT update failed.
#
# --------------------------------------------------------------------------------

trap tft.double.update.failed.trap

    # OID is for hanUpdAlert. For updates per se, we have just one alert.
    trap.oid .1.3.6.1.4.1.42536.4.1.0.1
    
    message.selector hanover.htc.device.update.status where (device.type = "tft") && (device.name = "tft.double")

    # Condition: !hanUpdSuccess
    trigger.condition !update.table[6, 6]
    trigger.type leading.edge
    
    # Associated: hanUpdWhen
    associated.object update.table[6, 5]

    # Associated: hanUpdDeviceType
    associated.object update.table[6, 2]

    # Associated: ht5SerialNumber (alt. could use hanUpdSerialNumber (update.table[5, 3]).
    associated.object tft.table[2, 2]

    # Associated: hanUpdUpdateType
    associated.object update.table[6, 4]

    # Associated: obcHtcVehicleID
    associated.object obc.table[1, 25]

end

# --------------------------------------------------------------------------------
#
#                            Trap: Data update failed.
#
# --------------------------------------------------------------------------------

trap data.update.failed.trap

    # OID is for hanUpdAlert. For updates per se, we have just one alert.
    trap.oid .1.3.6.1.4.1.42536.4.1.0.1
    
    message.selector hanover.htc.device.update.status where (device.type = "obc") && (update.type = "data")

    # Condition: !hanUpdSuccess
    trigger.condition !update.table[7, 6]
    trigger.type leading.edge
    
    # Associated: hanUpdWhen
    associated.object update.table[7, 5]

    # Associated: hanUpdDeviceType
    associated.object update.table[7, 2]

    # Associated: ht5SerialNumber (alt. could use hanUpdSerialNumber (update.table[5, 3]).
    associated.object obc.table[1,2]

    # Associated: hanUpdUpdateType
    associated.object update.table[7, 4]

    # Associated: obcHtcVehicleID
    associated.object obc.table[1, 25]

end

# --------------------------------------------------------------------------------
