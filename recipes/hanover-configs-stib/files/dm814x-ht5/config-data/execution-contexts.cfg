# ==========================================================================
#
#                     Execution Context Definitions
#
# --------------------------------------------------------------------------
#
# An execution context is an execution container that can cycle one-or-more
# features at some regular (defined) interval. 
#
# An execution context is typically a thread (though later external processes
# might/will also be supported).
#
# We can defined an arbitrary number of them here, each with it's own name. 
#
# If no contexts are defined (or if this file is missing) then a single
# default-default context is implicitly assumed and shared across all
# possible feature instantiations (i.e. all features will run in a single
# thread at some inbuild cycle interval).
#
# If a context named "default" is named here, then it will override the
# in-built default-default.
#
# Other execution-contexts can also be defined. There are here a few
# predefined ones "slow", "normal" and "fast", but only keep/use them if
# deemed appropriate (they at least serve as illustration).
# Even if you do not use them, they do no harm by still being defined here.
#
# Another configuration file, "execution-model.cfg" can be used to assign
# specific feature (instances) to specific execution contexts. For any
# feature that has no such specification, it will be run in the explicitly
# define "default" context or (if no such default is defined here) the
# in-built default-default context otherwise.
#
# If you wish two distinct feature instances to run at the same rate, but
# in different threads, simply define two execution contexts (with 
# different names) but with the same interval defined. Then in the
# "execution-model.cfg" file, assign the two feature instances to each of the
# two execution contexts.
#
# If you wish several feature instances to execute serially, one after the
# other within a given cycle, then just assign all to the same execution
# context.
#
# The existence of these contexts should prove useful for final performance
# tuning as we can tweak the balance of CPU usage between features without
# having to recompile code. The proportion of CPU that each feature gets
# (and when it gets it) in relation to the other features is more deterministic
# and better defined that if we were to rely upon OS scheduling alone.
#
# Feel free to edit this file however and to define your own contexts.
#
# (Any line beginning with a '#' is a comment line.)
#
# SJM - 2014-01-05
#
# ==========================================================================
#
# Let's define a reasonable default. This will be used to execute any
# features for which we do not explicitly specify an execution context.
#
# Once every two seconds is a bit lacklustre, but at least sure any feature
# gets executed (albeit infrequently) even when its execution model has
# not been explicitly considered/defined. We use a longish interval here to
# encourage some explicit thought about each feature's execution model,
# while still ensuring it does execute (albeit infrequently).
#
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

context default
    type thread
    interval 00:00:02
end

# --------------------------------------------------------------------------
#
# Here we define a context for less important features that do not need to
# execute too often. Once every ten seconds. Seems reasonable.
#
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

context slow 
    type thread
    interval 00:00:10.000
end

# --------------------------------------------------------------------------
#
# Here we define a middle-ground execution context, perhaps to be used as
# an explicit starting point for any new feature instantiation?
#
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

context normal
    type thread
    interval 00:00:02.000
end

# --------------------------------------------------------------------------
#
# Stuff that needs fairly frequent execution. Many stock system components
# will typically be configured to use this (e.g. when using the stock 
# console feature, or record/replay features, we typically want them to
# respond quickly to any incoming messages).
#
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

context fast 
    type thread
    interval 00:00:00.5
end

# --------------------------------------------------------------------------
#
# Define an execution context for timeshift features. We want these to be
# nice and responsive and to counteract aliasing, we make this twice as
# frequent as any other execution context.
#
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

context timeshift
    type thread
    interval 00:00:00.25
end

# --------------------------------------------------------------------------
#
# Here we define a context for status updates and other such very infrequent
# things. These really do not need to be run very often. Once a minute is
# probably still way too long.
#
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

context background
    type thread
    interval 00:01:00
end

# --------------------------------------------------------------------------
#
# Here we define an execution context, to handle features dealing with
# the flow of a destination code.
#
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

context flow
    type thread
    interval 00:00:00.5
end

# --------------------------------------------------------------------------
#
# Here we define some contexts within which to execute any feature instance
# whose sole purpose is to be the source of information to be published
# to SNMP.
#
# We also provide here a context within which to run the SNMP agent feature
# that will take this information and expose it via SNMP.
#
# --------------------------------------------------------------------------

context snmpdaily
    type thread
    stack 8KiB
   #interval 24:00:00
    interval 00:10:00
end

context snmphourly
    type thread
    stack 8KiB
   #interval 01:00:00
    interval 00:05:00
end

context snmpminutely
    type thread
    stack 8KiB
   #interval 00:01:00
    interval 00:01:00
end

context snmpagent
    type thread
    stack 12KiB
    # Ideally, no longer than half the interval of the shortest
    # of all of the above SNMP contexts.
    interval 00:00:15
end

# --------------------------------------------------------------------------


