# ========================================================================
#
# This is the application logging configuration file.                        
#
# You will need some knowledge of the code to add/remove items from    
# here.                                                                
#
# However, for non-developers, there should be included here (by       
# developers) aspects of specific configuration, fully commented to    
# document what useful information it logs and why it is useful.       
#
# The aim is to increase the degree of diagnostic capability that can  
# be performed without involving poor overworked developers.  :)       
#
# Note to developers: If you turn on "debug" or "verbose" logging for your
# feature during development, please be careful not to commit this to the
# repository as it makes the log output very noisy and we tend to run out
# of cpu power on the boards.
# ========================================================================


# ========================================================================
#
# This is the default logging configuration.
#
# By default we only want to know about errors and warnings.
#
# We send it to stdout as (by default) we dont want to consume persistent
# storage space.
#
# ------------------------------------------------------------------------

[default]
level=warn
target=stdout
format=%DU %TU+ (%t) [%L] %M {%C}

# ========================================================================
#
# 
#
# ------------------------------------------------------------------------

[app.main]
level=info
target=stdout
format=%DU %TU+ (%t) [%L] %M

# ========================================================================

[hanover.htc.obc.network.identity]
level=none
target=stdout
format=%DU %TU+ (%t) %{red}[%L]%{revert} %M (%C)

[hanover.network.networkidentityinquirer]
level=none
target=stdout
format=%DU %TU+ (%t) %{magenta}[%L]%{revert} %M (%C)


# ========================================================================
#
# 
#
# ------------------------------------------------------------------------

[hanover.htc.obc.cpu.loading]
level=none
target=stdout
format=%DU %TU+ (%t) [%L] %{magenta}%M%{revert}

# ========================================================================
#
# Useful for witnessing retrieval of update status info from the
# associated update status file.
#
#   Level Info:
#     - shows what update status info it retrieved and from what file.
#
#   Level Debug:
#     - also shows failed file reads.
#
#   Level Verbose:
#     - shows whenever about to attempt an update status file read.
#
# ------------------------------------------------------------------------

[hanover.htc.device]
level=info
target=stdout
format=%DU %TU+ (%t) [%L] %{magenta}%M%{revert} (%C)

# ========================================================================
#
# This is useful for seeing how a system is configured and how that
# results in specific feature instantiation.
#
#   Level Info:
#     - what features are installed (including build-in/stock features)
#     - which features are actually instantiated and hosted
#     - how each instantiated feature is configured (parameters etc.)
#     - the fully qualified identity of each feature.
#
# ------------------------------------------------------------------------

[hanover.messaging.hosting.hosthub]
level=info
target=stdout
format=%DU %TU+ (%t) [%L] %{red}%M%{revert}

# ========================================================================
#
# This is useful for viewing how each message is routed and optionally
# the decison-making in deciding where it is routed.
#
#   Level Info: shows each message as it is actually routed, revealing
#       both source and target features.
#
#   Level Debug: shows feature selector details for routed messages
#       and any details of how/why a message may not arrive at some
#       destined feature.
#
# ------------------------------------------------------------------------

[hanover.messaging.routing.messagerouter]
level=debug
target=stdout
format=%DU %TU+ (%t) [%L] %{red}%M%{revert}

# ========================================================================
#
# Just in case you need to inspect the routing rules being parsed, perhaps
# to see in detail whats happening should the routing rules be not working
# as expected.
#
# ------------------------------------------------------------------------

[hanover.messaging.routing.messageroutingparser]
level=none
target=stdout
format=%DU %TU+ (%t) [%L] %{green}%M%{revert}

# ========================================================================
#
# This shows which feature instantiations are contained within which
# execution context.
#
#   Level Info: shows feature execution context assignments.
#
# ------------------------------------------------------------------------

[hanover.messaging.executionstimuli.flexithreadedfeaturestimulus]
level=none
target=stdout
format=%DU %TU+ (%t) [%L] %{cyan}%M%{revert}

# ========================================================================
#
# This is useful for seeing how each feature instance has been
# compartmentalised into each execution context (i.e. thread).
#
#   Level Info: shows each feature instance as it is added to its
#       associated context.
#
# ------------------------------------------------------------------------

#[hanover.messaging.executionstimuli.threadedfeaturegroup]
#level=none
#target=stdout
#format=%DU %TU+ (%t) [%L] %{red}%M%{revert}

# ========================================================================
#
# The SNMP Agent feature. This marshalls the relevant incoming messages
# to sn SNMP message processor that does the real work.
#
# ------------------------------------------------------------------------

[hanover.htc.snmp.agent]
level=none
target=stdout
format=%DU %TU+ (%t) [%L] %{cyan}%M%{revert}  {%C}

# ========================================================================
#
# The SNMP message processor. This is presented with incoming messages,
# will decide whether or not an SNMP item needs updating from information
# in the message, will extract that data and present it to the low-level
# SNMP API. It may also trigger an SNMP trap.
#
# ------------------------------------------------------------------------

[hanover.productbase.htc.stockfeatures.snmp.snmpmessageprocessor]
level=debug
target=stdout
format=%DU %TU+ (%t) [%L] %{cyan}%M%{revert}  {%C}

# ========================================================================
#
#
#
# ------------------------------------------------------------------------

[hanover.productbase.htc.stockfeatures.snmp.snmpmessagebindings]
level=info
target=stdout
format=%DU %TU+ (%t) [%L] %{cyan}%M%{revert}  {%C}

# ========================================================================
#
# The SNMP AgentX low-level API. This is a wrapper around net-snmp for
# making those low-level SNMP calls exposing data to SNMP clients.
#
# ------------------------------------------------------------------------

[hanover.productbase.htc.stockfeatures.snmp.snmpagentx]
level=verbose
target=stdout
format=%DU %TU+ (%t) [%L] %{green}%M%{revert}  {%C}


# ========================================================================
#
# SNMP expression evaluation. Identifiers are interpreted to be properties
# of a presented message.
#
# ------------------------------------------------------------------------

[hanover.messaging.propertyexpressioncompiler]
level=none
target=stdout
format="%DU %TU+ (%t) [%L] %{red}%M%{revert} {%C}"

# ========================================================================
#
# SNMP expression evaluation. Identifiers are interpreted to be
# defined SNMP items (and table elements) as defined in an SNMP
# definitions file (e.g. snmp-scalars.cfg or snmp-tables.cfg).
#
# ------------------------------------------------------------------------

[hanover.productbase.htc.stockfeatures.snmp.snmpexpressioncompiler]
level=none
target=stdout
format="%DU %TU+ (%t) [%L] %{red}%M%{revert} {%C}"

# ========================================================================
#
# Generic expression evaluation: building the expression tree. Caters
# correctly for (specified) operator precedence, brackets, etc.
#
# ------------------------------------------------------------------------

[hanover.expressions.parsing.expressionshuntingyardparser]
level=none
target=stdout
format="%DU %TU+ (%t) [%L] %{red}%M%{revert} {%C}"

# ========================================================================
#
# Generic expression evaluation: lexical analysis. 
#
# ------------------------------------------------------------------------

[hanover.expressions.parsing.expressionlexicalanalyser]
level=none
target=stdout
format="%DU %TU+ (%t) [%L] %{red}%M%{revert} {%C}"

# ========================================================================
#
#
#
# ------------------------------------------------------------------------

[hanover.productbase.htc.stockfeatures.snmp.snmpstandardscalardefinitionsreader]
level=none
target=stdout
format=%DU %TU+ (%t) [%L] %{blue}%M%{revert}  {%C}

# ========================================================================
#
#
#
# ------------------------------------------------------------------------

[hanover.productbase.htc.stockfeatures.snmp.snmpstandardtabledefinitionsreader]
level=none
target=stdout
format=%DU %TU+ (t%) [%L] %{blue}%M%{revert}  {%C}

# ========================================================================
#
#
#
# ------------------------------------------------------------------------

[hanover.productbase.htc.stockfeatures.snmp.snmpstandardbindingsreader]
level=none
target=stdout
format=%DU %TU+ (%t) [%L] %{blue}%M%{revert}  {%C}

# ========================================================================
#
#
#
# ------------------------------------------------------------------------

[hanover.productbase.htc.stockfeatures.snmp.snmpstandardmessagebindingsparser]
level=none
target=stdout
format=%DU %TU+ (%t) [%L] %{blue}%M%{revert}  {%C}

# ========================================================================
#
#
#
# ------------------------------------------------------------------------

[hanover.productbase.htc.stockfeatures.snmp.snmpstandardtrapsreader]
level=none
target=stdout
format=%DU %TU+ (%t) [%L] %{blue}%M%{revert}  {%C}

# ========================================================================
#
#
#
# ------------------------------------------------------------------------

[hanover.productbase.htc.stockfeatures.snmp.snmprepositorybase]
level=none
target=stdout
format=%DU %TU+ (%t) [%L] %{blue}%M%{revert}  {%C}

# ========================================================================
#
# Stibis Translator
#
# ------------------------------------------------------------------------

[hanover.htc.stibistranslator]
level=none
target=stdout

# ========================================================================
#
# Stimulus Collator
#
# ------------------------------------------------------------------------

[hanover.htc.stimuluscollator]
level=none
target=stdout

# ========================================================================
#
# Journey
#
# ------------------------------------------------------------------------

[hanover.htc.Journey]
level=none
target=stdout

# ========================================================================
#
# Response Controller
#
# ------------------------------------------------------------------------

[hanover.htc.responsecontroller]
level=none
target=stdout


