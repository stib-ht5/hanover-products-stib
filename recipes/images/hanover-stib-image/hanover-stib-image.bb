# vim: set ft=sh:
require ${OEBASE}/hanover-system/recipes/images/hanover-application-image.inc

PR = "${INC_PR}.1"

COMPATIBLE_MACHINE = "(dm365-htc|dm814x-ht5)"

# 900MiB ext3 rootfs on HT2/HT3
IMAGE_FSTYPES_dm365-htc     = "ext3"
IMAGE_ROOTFS_SIZE_dm365-htc = 921600

SRC_URI += "\
	file://mkupdate.sh	\
	file://release.sh	\
	"
SRC_URI_append_dm365-htc = " file://mksdimage.sh"

do_deploy_append() {
	install -m 0755 ${WORKDIR}/*.sh ${DEPLOY_DIR_IMAGE}/scripts/
}
