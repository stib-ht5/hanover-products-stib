#!/bin/sh

set -eu
RELEASE_DIR=$(dirname "$0")/..
MKUPDATEPY=$(dirname "$0")/mkupdate.py

get_field() {
	sed -ne "s|^$1: *\(.*\)|\1|p" "$RELEASE_DIR/versions.txt"
}

MACHINE=$(get_field 'Machine')
PRODUCT=$(get_field 'Product ID')
PRODUCT_RELEASE=$(get_field 'Product release')
BUILD_TYPE=$(get_field 'Build type')

$MKUPDATEPY $RELEASE_DIR/opkg_status.txt "$@"
ls -1 $RELEASE_DIR/update-from-*/ipk/Packages.gz | sed \
	-e 's|/ipk/Packages.gz$||' -e 's|.*/update-from-||' | while read base; do

	update_dir="$RELEASE_DIR/update-from-$base"
	baserelease="$update_dir/ipk/baserelease"
	d="$PRODUCT.$PRODUCT_RELEASE/$BUILD_TYPE"

	if [ -s "$baserelease" ]; then
		echo "$update_dir: skippping" >&2
	else
		f="$update_dir/$PRODUCT.$PRODUCT_RELEASE.@MD5@.tar"
		rm -f "$update_dir"/$PRODUCT.$PRODUCT_RELEASE.*.tar
		rm -f "$update_dir"/$PRODUCT.$PRODUCT_RELEASE.*.tar.bz2

		mkdir -p "$update_dir/$d"
		ln -sndf ../../ipk "$update_dir/$d/"

		# make old versions of update.sh happy by creating dummy "Packages" files
		find "$update_dir/ipk" -name Packages.gz | while read f; do
			touch "${f%.gz}"
		done

		echo -n $base > "$baserelease"
		cp "$RELEASE_DIR/opkg_status.txt" "$update_dir/ipk/$MACHINE/opkg.status"

		tar -C "$update_dir" --owner=root -cvhf "$f" "$d"
		bzip2 -1 "$f"
		hash=$(md5sum "$f.bz2" | cut -d' ' -f1)
		f1="$(echo "$f.bz2" | sed -e "s|@MD5@|$hash|g")"
		mv "$f.bz2" "$f1"
		du -h "$f1"
	fi

done
