#!/bin/sh

. "$(dirname "$0")/build_release.inc"

load_root_image

# card sizes
[ $# -gt 0 ] || set -- 1900

for x; do
	has_data=false

	for y in ${RELEASE_DIR:-$DEPLOY_IMAGE_DIR}/data*.zip \
		 ${RELEASE_DIR:-$DEPLOY_IMAGE_DIR}/data*.tar.gz; do
		if [ -s "$y" ]; then
			has_data=true
			mksdimage_ht2 "$x" "$y"
		fi
	done
	$has_data || mksdimage_ht2 "$x"

	title "Done"
	unmount_image
done

unload_root_image
