# vim: set ft=sh noet ts=8 sw=8:
require recipes/tasks/task-hanover-image.inc

PV = "${PRODUCT_RELEASE}"
PR = "${INC_PR}.3"

RDEPENDS_${PN} += "\
	hanover-update-${PRODUCT}		\
	hanover-product-${PRODUCT}		\
	"

RDEPENDS_${PN} += "\
	stib-htc		\
	nfsguard		\
	initscript-telnetd	\
	"

RRECOMMENDS_${PN} += "\
	task-hanover-image	\
"
