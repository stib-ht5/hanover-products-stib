################################################################################
# SR1106 library list
#
# Author: Andrei Mironenko <amironenko@hanoverdisplays.com>
################################################################################

# Library packages can be build as release or development. 
# Development library build additionally includes documentation and header files   
# and usually used for SDK.
# LIB_BUILD_MODE denotes either we are building libraris for a product image or 
# product SDK, release or dev. version correspondenly.

 
pf=${LIB_BUILD_MODE}

TASK_HANOVER_GLIBC  =          "\
"

TASK_HANOVER_LIBS   =         "\
"
TASK_HANOVER_NET_SNMP =       "\
   net-snmp-static             \         
   net-snmp-dev                \
"

TASK_HANOVER_BOOST_LIBS   =     "\
"

TASK_HANOVER_SDL_LIBS =        "\
  "

TASK_HANOVER_NCURSES_LIBS =     "\
  "
  
TASK_HANOVER_GL_LIBS_dm814x-evm =  "\
    "
TASK_HANOVER_GL_LIBS=""


#    libavahi-client3       
#    libavahi-common3       
#    libavahi-core6         
#    libdaemon0            
 
TASK_HANOVER_AVAHI_LIBS = "\
    libnss-mdns            \
"
   

TASK_HANOVER_APPS_LIBS =        "\
  jsoncpp${pf}                   \
  sqlitewrapped${pf}             \
  hasl${pf}                      \
  net-snmp-agentx++${pf}         \
  python-bitarray${pf}           \
  python-hanover${pf}            \
  python-statemachine${pf}       \
  python-stib${pf}               \
  python-singletonmixin${pf}     \ 
"

export TASK_HANOVER_PRODUCT_LIBS =  "\
    ${TASK_HANOVER_GLIBC}           \
    ${TASK_HANOVER_LIBS}            \
    ${TASK_HANOVER_NET_SNMP}        \
    ${TASK_HANOVER_BOOST_LIBS}      \
    ${TASK_HANOVER_SDL_LIBS}        \
    ${TASK_HANOVER_NCURSES_LIBS}    \
    ${TASK_HANOVER_GL_LIBS}         \
    ${TASK_HANOVER_AVAHI_LIBS}      \
    ${TASK_HANOVER_APPS_LIBS}       \
"

###############################################################################
#   Preferred versions must be define in this section                         #
###############################################################################
PREFERRED_VERSION_jsoncpp${pf} = "0.5.0"
PREFERRED_VERSION_sqlitewrapped = "1.3.1"
PREFERRED_VERSION_libgles-omap3 = "4.04.00.02"
PREFERRED_VERSION_hasl = "1.1.4"
PREFERRED_VERSION_net-snmp-agentx++ = "1.0.7"


# Python libraries
PREFERRED_VERSION_python-bitarray = "1.0.2"
PREFERRED_VERSION_python-hanover = "1.0.2"
PREFERRED_VERSION_python-statemachine = "1.0.10"
PREFERRED_VERSION_python-stib = "1.0.15"
PREFERRED_VERSION_python-singletonmixin = "1.0.1"
