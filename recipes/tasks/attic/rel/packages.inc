################################################################################
# Project STIB package list
#
# Author: Andrei Mironenko <amironenko@hanoverdisplays.com>
#
# Copyright(C) 2015 Hanover Displays Ltd.
# This file is licensed under the terms of the GNU General Public License
# version 2. This program  is licensed "as is" without any warranty of any kind,
# whether express or implied.
################################################################################

TASK_HANOVER_BASE =      "\
    initscript-telnetd    \
  "
  
TASK_HANOVER_ALSA_UTILS = "\
    alsa-server \
    alsa-state \
    alsa-utils-midi \
    alsa-utils-aconnect \
    alsa-utils-iecset \
    alsa-utils-speakertest \
    alsa-utils-aseqnet \
    alsa-utils-aseqdump \
    alsa-utils-alsaconf \
    alsa-utils-alsactl  \
    "

TASK_HANOVER_UTILS =     "\
   fbset-modes            \
   diffutils              \
   diffstat               \
   patch                  \
   bzip2                  \
   quilt                  \
"

TASK_HANOVER_NTP =       "\
"

TASK_HANOVER_EDITORS =   "\
    "

TASK_HANOVER_JPEG_VIEWER = "\
  fbida                   \
  tiff                    \
  "
  
TASK_HANOVER_PYTHON = "\
python-cupshelpers \
python-mysqldb \
python-pycairo \
python-pycups \
python-pygobject \
python-pygtk \
python-smtpd \
python-tkinter \
"
  
TASK_HANOVER_FSUTILS =   "\
  "
  
TASK_HANOVER_GST = "\
"

TASK_HANOVER_NCURSES =   "\
    "
    
TASK_HANOVER_AVAHI = "\
    avahi-autoipd     \
    avahi-daemon      \
    avahi-utils       \
    zeroconf          \
"
    
TASK_HANOVER_ROUTER = "\
"	

TASK_HANOVER_APPS =      "\
  stib-htc                \
  nfsguard		  \
  "      
  
export TASK_HANOVER_PRODUCT =       "\
      ${TASK_HANOVER_BASE}          \
      ${TASK_HANOVER_UTILS}         \
      ${TASK_HANOVER_ALSA_UTILS}    \ 
      ${TASK_HANOVER_EDITORS}       \
      ${TASK_HANOVER_JPEG_VIEWER}   \
      ${TASK_HANOVER_NTP}           \
      ${TASK_HANOVER_FSUTILS}       \
      ${TASK_HANOVER_GST}           \
      ${TASK_HANOVER_NCURSES}       \
      ${TASK_HANOVER_PYTHON}        \
      ${TASK_HANOVER_AVAHI}         \
      ${TASK_HANOVER_APPS}          \
      ${TASK_HANOVER_ROUTER}        \
    "    

###############################################################################
# Hanover common packages preferred versions                                  #
###############################################################################
PREFERRED_VERSION_hanover-configs = "1.2.3"
PREFERRED_VERSION_hanover-security = "1.0.2"
PREFERRED_VERSION_hanover-update = "2.0.0"
PREFERRED_VERSION_hanover-update-stib = "1.2.2"
PREFERRED_VERSION_build-scripts = "1.2.2"

###############################################################################
# Application package preferred versions                                      #
###############################################################################
PREFERRED_VERSION_stib-htc = "1.0.19"

###############################################################################
# Machine specific preferred versions                                         #
###############################################################################

#TI DM814X packages
PREFERRED_VERSION_ti-media-controller-loader = "3.00.00.07"
PREFERRED_VERSION_ti-c674x-aaclcdec = "01.41.00.00"
PREFERRED_VERSION_ti-codec-engine = "3.22.01.06"
PREFERRED_VERSION_ti-edma3lld = "02.11.05.02"
PREFERRED_VERSION_ti-framework-components = "3.22.01.07"
PREFERRED_VERSION_ti-ipc = "1.24.03.32"
PREFERRED_VERSION_ti-linuxutils = "3.22.00.02"
PREFERRED_VERSION_ti-osal = "1.22.01.09"
PREFERRED_VERSION_ti-slog = "04.00.00.02"
PREFERRED_VERSION_ti-sysbios = "6.33.05.46"
PREFERRED_VERSION_ti-uia = "1.01.01.14"
PREFERRED_VERSION_ti-xdctools = "3.23.03.53"
PREFERRED_VERSION_ti-xdais = "7.22.00.03"
PREFERRED_VERSION_ti-rpe = "1.00.00.12"
PREFERRED_VERSION_ti-dspbios = "6.33.05.46"

#Upgraded 
PREFERRED_VERSION_gst-openmax-ti = "07.00.00"
PREFERRED_VERSION_ti-omx = "05.02.00.48"
PREFERRED_VERSION_ti-syslink = "2.20.02.20"
PREFERRED_VERSION_ti-cgt6x = "7.3.4"

#System
#DM814x-STIB
PREFERRED_VERSION_linux-omap3-evm = "2.6.37-04.04.00.01"
PREFERRED_VERSION_u-boot-omap3-evm = "04.04.00.01"
PREFFERED_VERSION_omap3-sgx-modules = "1.6.16.4117"

#DM814x-HT5
PREFERRED_VERSION_linux-omap3-ht5 = "2.6.37.6-HDL-HT5-20161025"
PREFERRED_VERSION_u-boot-omap3-ht5 = "2010.06_HDL_HT5_20160911"
PREFFERED_VERSION_omap3-sgx-modules = "1.6.16.4117"

#DM365-HTC
PREFERRED_VERSION_linux_davinci_htc = "2.6.32.71-HDL-HT2-20161018"
PREFERRED_VERSION_u-boot-davinci-htc = "03.21.00.04"
PREFERRED_VERSION_watchdog="1.2.3"
