################################################################################
# SR1106 library list
#
# Author: Andrei Mironenko <amironenko@hanoverdisplays.com>
################################################################################

# Development build is based on release one with some packages which are taken 
# from the cutting edge of the Git sources
require ../rel/libs.inc

################################################################################
# Git cutting edge version of libraries                                        #
################################################################################
PREFERRED_VERSION_jsoncpp${pf} = "git"

################################################################################
# Python libraries                                        #
################################################################################
PREFERRED_VERSION_python-bitarray${pf}     = "git"
PREFERRED_VERSION_python-statemachine${pf} = "git"
PREFERRED_VERSION_python-hanover${pf}      = "git"
PREFERRED_VERSION_python-stib${pf}         = "git"
