################################################################################
# SR1106 package list
#
# Author: Andrei Mironenko <amironenko@hanoverdisplays.com>
################################################################################


# Development build is based on the release one with some packages which are  
# taken from the cutting edge of the Git sources.
require ../rel/packages.inc

###############################################################################
# Hanover common packages preferred versions                                  #
###############################################################################
PREFERRED_VERSION_hanover-configs = "git"
PREFERRED_VERSION_hanover-security = "git"
PREFERRED_VERSION_build-scripts = "git"   

###############################################################################
# Application package preferred versions                                      #
###############################################################################
PREFERRED_VERSION_sdltest = "git"
PREFERRED_VERSION_iptft = "git"
PREFERRED_VERSION_firmloader = "git"
PREFERRED_VERSION_stib-htc = "git"                
PREFERRED_VERSION_stib-htc-config-data = "git"    
PREFERRED_VERSION_python-scripts = "git"         

###############################################################################
# Machine specific preferred versions                                         #
###############################################################################

#DM814x-STIB
PREFERRED_VERSION_linux-omap3-evm = "git"
PREFERRED_VERSION_u-boot-omap3-evm = "git"

#DM365-HTC
PREFERRED_VERSION_linux_davinci_htc = "git"
PREFERRED_VERSION_u-boot-davinci-htc = "git"
