cd /home/root
echo "Terminating App"
kill `pidof STIBApp`
echo "Terminating scripts..."
kill `pidof -x /usr/bin/templateFiller.py`
kill `pidof -x /usr/bin/serve_templates.py`
echo "Stopped STIB Application"
