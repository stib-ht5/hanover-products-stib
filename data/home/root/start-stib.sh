cd /home/root
echo "Terminating App"
kill `pidof STIBApp`
echo "Terminating scripts..."
kill `pidof -x /usr/bin/templateFiller.py`
kill `pidof -x /usr/bin/serve_templates.py`
export TZ=CET-1CEST,M3.5.0/2,M10.5.0/3
export HTC_CONFIG_SEARCH_PATH=.:~/.htc:/usr/local/config-data:/etc/config-data
export LOGGING_CONFIG_SEARCH_PATH=/media/removable1/log-config:/usr/local/config-data
STIBApp & templateFiller.py & serve_templates.py --keepalive 30 &
