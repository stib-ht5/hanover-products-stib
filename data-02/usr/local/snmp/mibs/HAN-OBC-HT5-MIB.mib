HAN-OBC-HT5-MIB DEFINITIONS ::= BEGIN

IMPORTS
    hanMIB, hanObcClass
        FROM HAN-ROOT-MIB
    MODULE-COMPLIANCE, NOTIFICATION-GROUP, OBJECT-GROUP
        FROM SNMPv2-CONF
    Integer32, IpAddress, MODULE-IDENTITY, NOTIFICATION-TYPE,
    OBJECT-IDENTITY, OBJECT-TYPE, TimeTicks
        FROM SNMPv2-SMI
    MacAddress, TruthValue, DateAndTime
        FROM SNMPv2-TC
    ;

obcHt5MIB MODULE-IDENTITY
    LAST-UPDATED  "201607150954Z" -- July 07, 2016
    ORGANIZATION
        "Hanover Displays Limited"
    CONTACT-INFO
        "postal: David Glendining
         Hanover Displays Limited
         Southerham House
         Southerham Lane
         Lewes
         East Sussex
         BN8 6JN
         email: dglendining@hanoverdisplays.com"
    DESCRIPTION
        "MIB for HT5 on board computer"
    REVISION    "201607150954Z" -- July 07, 2016
    DESCRIPTION
        "Addition of new firmware versions"
    REVISION    "201409231621Z" -- September 23, 2014
    DESCRIPTION
        "Revisions to improve Trap informativeness"
    ::= { hanMIB 2 }

obcHt5Devices OBJECT-IDENTITY
    STATUS        current
    DESCRIPTION
        "HT5 device objects"
    ::= { hanObcClass 1 }

obcHt5Table OBJECT-TYPE
    SYNTAX        SEQUENCE OF ObcHt5Entry
    MAX-ACCESS    not-accessible
    STATUS        current
    DESCRIPTION
        "Table of HT5 devices in the system"
    ::= { obcHt5Devices 1 }

obcHt5Entry OBJECT-TYPE
    SYNTAX        ObcHt5Entry
    MAX-ACCESS    not-accessible
    STATUS        current
    DESCRIPTION
        "An HT5 instance"
    INDEX
        { obcHt5Index }
    ::= { obcHt5Table 1 }

ObcHt5Entry ::= SEQUENCE
{
    obcHt5Index           INTEGER,
    obcHt5SerialNumber    OCTET STRING,
    obcHt5MACAddress1     MacAddress,
    obcHt5MACAddress2     MacAddress,
    obcHt5IPv4Address1    IpAddress,
    obcHt5IPv4Address2    IpAddress,
    obcHt5VersionHardware OCTET STRING,
    obcHt5VersionOS       OCTET STRING,
    obcHt5VersionSoftware OCTET STRING,
    obcHt5Voltage         Integer32,
    obcHt5Uptime          TimeTicks,
    obcHt5CpuUsage1min    Integer32,
    obcHt5CpuUsage5min    Integer32,
    obcHt5CpuUsage15min   Integer32,
    obcHt5ProcessCount    Integer32,
    obcHt5CpuTemp         Integer32,
    obcHt5AmpTemp         Integer32,
    obcHt5AmbientTemp     Integer32,
    obcHt5DiskRemaining   Integer32,
    obcHt5DiskTotal       Integer32,
    obcHt5RamRemaining    Integer32,
    obcHt5RamTotal        Integer32,
    obcHt5NeedSwap        TruthValue,
    obcHt5ErrorText       OCTET STRING,
    obcHt5VehicleID       OCTET STRING,
    obcHt5TimeStamp       DateAndTime,
    obcHt5VersionHotApp   OCTET STRING,
    obcHt5VersionHotBoot  OCTET STRING,
    obcHt5VersionColdApp  OCTET STRING,
    obcHt5VersionColdBoot OCTET STRING
}


obcHt5Index OBJECT-TYPE
    SYNTAX        Integer32 (1..2147483647)
    MAX-ACCESS    not-accessible
    STATUS        current
    DESCRIPTION
        "Index of table entry"
    ::= { obcHt5Entry 1 }

obcHt5SerialNumber OBJECT-TYPE
    SYNTAX        OCTET STRING (SIZE(1..32))
    MAX-ACCESS    read-only
    STATUS        current
    DESCRIPTION
        "Product Serial Number"
    ::= { obcHt5Entry 2 }

obcHt5MACAddress1 OBJECT-TYPE
    SYNTAX        MacAddress
    MAX-ACCESS    read-only
    STATUS        current
    DESCRIPTION
        "MAC Address of first ethernet port"
    ::= { obcHt5Entry 3 }

obcHt5MACAddress2 OBJECT-TYPE
    SYNTAX        MacAddress
    MAX-ACCESS    read-only
    STATUS        current
    DESCRIPTION
        "MAC Address of second ethernet port"
    ::= { obcHt5Entry 4 }

obcHt5IPv4Address1 OBJECT-TYPE
    SYNTAX        IpAddress
    MAX-ACCESS    read-only
    STATUS        current
    DESCRIPTION
        "IPv4 address of first ethernet port"
    ::= { obcHt5Entry 5 }

obcHt5IPv4Address2 OBJECT-TYPE
    SYNTAX        IpAddress
    MAX-ACCESS    read-only
    STATUS        current
    DESCRIPTION
        "IPv4 address of second ethernet port"
    ::= { obcHt5Entry 6 }

obcHt5VersionHardware OBJECT-TYPE
    SYNTAX        OCTET STRING (SIZE(1..32))
    MAX-ACCESS    read-only
    STATUS        current
    DESCRIPTION
        "Hardware version"
    ::= { obcHt5Entry 7 }

obcHt5VersionOS OBJECT-TYPE
    SYNTAX        OCTET STRING (SIZE(1..32))
    MAX-ACCESS    read-only
    STATUS        current
    DESCRIPTION
        "Operating system version"
    ::= { obcHt5Entry 8 }

obcHt5VersionSoftware OBJECT-TYPE
    SYNTAX        OCTET STRING (SIZE(1..32))
    MAX-ACCESS    read-only
    STATUS        current
    DESCRIPTION
        "Application software version"
    ::= { obcHt5Entry 9 }

obcHt5Voltage OBJECT-TYPE
    SYNTAX        Integer32
    MAX-ACCESS    read-only
    STATUS        current
    DESCRIPTION
        "System input voltage in decivolts"
    ::= { obcHt5Entry 10 }

obcHt5Uptime OBJECT-TYPE
    SYNTAX        TimeTicks
    MAX-ACCESS    read-only
    STATUS        current
    DESCRIPTION
        "The time (in hundredths of a second) since the system was
         booted"
    ::= { obcHt5Entry 11 }

obcHt5CpuUsage1min OBJECT-TYPE
    SYNTAX        Integer32
    MAX-ACCESS    read-only
    STATUS        current
    DESCRIPTION
        "CPU usage permillage over the last minute"
    ::= { obcHt5Entry 12 }

obcHt5CpuUsage5min OBJECT-TYPE
    SYNTAX        Integer32
    MAX-ACCESS    read-only
    STATUS        current
    DESCRIPTION
        "CPU usage permillage over the last five minutes"
    ::= { obcHt5Entry 13 }

obcHt5CpuUsage15min OBJECT-TYPE
    SYNTAX        Integer32
    MAX-ACCESS    read-only
    STATUS        current
    DESCRIPTION
        "CPU usage permillage over the last fifteen minutes"
    ::= { obcHt5Entry 14 }

obcHt5ProcessCount OBJECT-TYPE
    SYNTAX        Integer32
    MAX-ACCESS    read-only
    STATUS        current
    DESCRIPTION
        "Number of processes"
    ::= { obcHt5Entry 15 }

obcHt5CpuTemp OBJECT-TYPE
    SYNTAX        Integer32
    MAX-ACCESS    read-only
    STATUS        current
    DESCRIPTION
        "CPU temperature in Celsius, scaled by 10"
    ::= { obcHt5Entry 16 }

obcHt5AmpTemp OBJECT-TYPE
    SYNTAX        Integer32
    MAX-ACCESS    read-only
    STATUS        current
    DESCRIPTION
        "Amplifier temperature in Celsius, scaled by 10"
    ::= { obcHt5Entry 17 }

obcHt5AmbientTemp OBJECT-TYPE
    SYNTAX        Integer32
    MAX-ACCESS    read-only
    STATUS        current
    DESCRIPTION
        "Ambient temperature in Celsius, scaled by 10"
    ::= { obcHt5Entry 18 }

obcHt5DiskRemaining OBJECT-TYPE
    SYNTAX        Integer32
    UNITS         "KiB"
    MAX-ACCESS    read-only
    STATUS        current
    DESCRIPTION
        "Amount of disk space remaining in KiB"
    ::= { obcHt5Entry 19 }

obcHt5DiskTotal OBJECT-TYPE
    SYNTAX        Integer32
    UNITS         "KiB"
    MAX-ACCESS    read-only
    STATUS        current
    DESCRIPTION
        "Total amount of disk space in KiB"
    ::= { obcHt5Entry 20 }

obcHt5RamRemaining OBJECT-TYPE
    SYNTAX        Integer32
    UNITS         "KiB"
    MAX-ACCESS    read-only
    STATUS        current
    DESCRIPTION
        "Amount of free RAM in KiB"
    ::= { obcHt5Entry 21 }

obcHt5RamTotal OBJECT-TYPE
    SYNTAX        Integer32
    UNITS         "KiB"
    MAX-ACCESS    read-only
    STATUS        current
    DESCRIPTION
        "Total amount of RAM in KiB"
    ::= { obcHt5Entry 22 }

obcHt5NeedSwap OBJECT-TYPE
    SYNTAX        TruthValue
    MAX-ACCESS    read-only
    STATUS        current
    DESCRIPTION
        "Item requires replacement"
    ::= { obcHt5Entry 23 }

obcHt5ErrorText OBJECT-TYPE
    SYNTAX        OCTET STRING (SIZE(0..1024))
    MAX-ACCESS    read-only
    STATUS        current
    DESCRIPTION
        "Text used in Notification to describe error(s)"
    ::= { obcHt5Entry 24 }

obcHt5VehicleID OBJECT-TYPE
    SYNTAX        OCTET STRING (SIZE(0..256))
    MAX-ACCESS    read-only
    STATUS        current
    DESCRIPTION
        "Field used to store the Vehicle Identification Number"
    ::= { obcHt5Entry 25 }

obcHt5TimeStamp OBJECT-TYPE
    SYNTAX        DateAndTime
    MAX-ACCESS    read-only
    STATUS        current
    DESCRIPTION
        "The timestamp (in Local time!) used when raising Traps"
    ::= { obcHt5Entry 26 }

obcHt5VersionHotApp OBJECT-TYPE
    SYNTAX        OCTET STRING (SIZE(1..32))
    MAX-ACCESS    read-only
    STATUS        current
    DESCRIPTION
        "Firmware version of hot side application"
    ::= { obcHt5Entry 27 }
    
obcHt5VersionHotBoot OBJECT-TYPE
    SYNTAX        OCTET STRING (SIZE(1..32))
    MAX-ACCESS    read-only
    STATUS        current
    DESCRIPTION
        "Firmware version of hot side bootloader"
    ::= { obcHt5Entry 28 }
    
obcHt5VersionColdApp OBJECT-TYPE
    SYNTAX        OCTET STRING (SIZE(1..32))
    MAX-ACCESS    read-only
    STATUS        current
    DESCRIPTION
        "Firmware version of cold side application"
    ::= { obcHt5Entry 29 }
    
obcHt5VersionColdBoot OBJECT-TYPE
    SYNTAX        OCTET STRING (SIZE(1..32))
    MAX-ACCESS    read-only
    STATUS        current
    DESCRIPTION
        "Firmware version of cold side bootloader"
    ::= { obcHt5Entry 30 }
 
obcHt5Notifications OBJECT-IDENTITY
    STATUS        current
    DESCRIPTION
        "HT5 related traps"
    ::= { obcHt5MIB 0 }

obcHt5Alert NOTIFICATION-TYPE
    OBJECTS
        { obcHt5TimeStamp, obcHt5SerialNumber, obcHt5ErrorText, obcHt5VehicleID }
    STATUS        current
    DESCRIPTION
        "HT5 requires attention"
    ::= { obcHt5Notifications 1 }

obcHt5Conformance OBJECT IDENTIFIER ::= { obcHt5MIB 2 }

obcHt5Compliances OBJECT IDENTIFIER ::= { obcHt5Conformance 1 }

obcHt5BasicCompliance MODULE-COMPLIANCE
    STATUS         current
    DESCRIPTION
        "The compliance statement for SNMP entities which implement
         this MIB module."
    MODULE         HAN-OBC-HT5-MIB
        MANDATORY-GROUPS
            { obcHt5BasicGroup }
        GROUP          obcHt5NotifyGroup
        DESCRIPTION
            "Mandatory for reporting of general failures"
    ::= { obcHt5Compliances 1 }

obcHt5Groups OBJECT IDENTIFIER ::= { obcHt5Conformance 2 }

obcHt5BasicGroup OBJECT-GROUP
    OBJECTS
        { obcHt5AmbientTemp, obcHt5AmpTemp, obcHt5CpuTemp, obcHt5CpuUsage1min,
        obcHt5CpuUsage5min, obcHt5CpuUsage15min, obcHt5DiskRemaining,
        obcHt5DiskTotal, obcHt5IPv4Address1, obcHt5IPv4Address2, obcHt5MACAddress1,
        obcHt5MACAddress2, obcHt5NeedSwap, obcHt5ProcessCount, obcHt5RamRemaining,
        obcHt5RamTotal, obcHt5SerialNumber, obcHt5Uptime, obcHt5VersionHardware,
        obcHt5VersionOS, obcHt5VersionSoftware, obcHt5Voltage, obcHt5ErrorText,
        obcHt5VehicleID, obcHt5TimeStamp, obcHt5VersionHotApp, obcHt5VersionHotBoot,
		obcHt5VersionColdApp, obcHt5VersionColdBoot}
    STATUS        current
    DESCRIPTION
        "The basic group defines the usual HT5 device table data"
    ::= { obcHt5Groups 1 }

obcHt5NotifyGroup NOTIFICATION-GROUP
    NOTIFICATIONS
        { obcHt5Alert }
    STATUS        current
    DESCRIPTION
        "Defines the primary failure notification mechanism"
    ::= { obcHt5Groups 2 }

END
