#!/bin/sh

set -eu
ME="$(readlink -f "$0")"
BASE="$(dirname "$ME")/../.."

PACKAGES="python avahi boost virtual/kernel libhtc-core"
PACKAGES="$PACKAGES stib-htc hanover-stib-image"

exec $BASE/bb.sh -p stib -l "$@" $PACKAGES
